import json
import os
from flask import Flask, render_template, request


app = Flask(__name__)
port = int(os.getenv('PORT', 5000))


@app.route("/")
def home():
    return render_template('index.html', searching=False)


@app.route("/search")
def search():
    word = request.values['word']
    with open('data.json', 'r') as f:
        data = json.load(f)
    if word in data:
        result = data[word]
    else:
        result = None
    return render_template(
        'index.html', result=result, word=word, searching=True
    )


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=port)
